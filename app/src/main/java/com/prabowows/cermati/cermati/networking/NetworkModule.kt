package id.astra.daihatsu.mobileservice.networking


import android.content.Context
import androidx.collection.LruCache
import com.example.agrprabo6477.ajobthing.utils.PreferenceManager
import com.prabowows.cermati.cermati.R
import com.prabowows.cermati.cermati.utils.HelpersJava
import com.readystatesoftware.chuck.ChuckInterceptor
import id.astra.ai.utils.Const
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.CertificatePinner
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import okhttp3.logging.HttpLoggingInterceptor



/**
 * Created by Prabowo Wahyu Sudarno on 3/4/2019.
 */

class NetworkModule {


    /**
     * Method to return the API interface.

     * @return
     */
    lateinit var service: NetworkService

    private lateinit var apiObservables: LruCache<Class<*>, Observable<*>>

    private val preferenceManager = PreferenceManager.instance

    private var requestSeamless : RequestBody? = null

    constructor()

//    constructor(context: Context, formBody: RequestBody, urlType: Int) : this(context, formBody)

//    constructor(context: Context, formBody: RequestBody, authorization: String, isLoginUrl: Boolean) {
//        requestSeamless = formBody
//        var url = ""
//        var okHttpClient = buildClient(context, formBody, authorization)
//        if (isLoginUrl) {
//            url = Const.urlLogin
//            okHttpClient = buildClient(context, formBody)
//        } else {
//            url = Const.url
//        }
//
//        apiObservables = LruCache<Class<*>, Observable<*>>(10)
//
//        val retrofit = Retrofit.Builder()
//                .baseUrl(url)
//                .addConverterFactory(GsonConverterFactory.create(HelpersJava.getCustomGson2(context)))
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                .client(okHttpClient)
//                .build()
//
//        service = retrofit.create(NetworkService::class.java)
//    }

    constructor(context: Context) {
        var url = Const.url
        var okHttpClient = buildClientGet(context)


        apiObservables = LruCache<Class<*>, Observable<*>>(10)

        val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(GsonConverterFactory.create(HelpersJava.getCustomGson(context)))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()

        service = retrofit.create(NetworkService::class.java)
    }

    constructor(context: Context, formBody: RequestBody, authorization: String, isLoginUrl: Boolean,isDrapdev : Boolean) {
        var url = "http://a000s-drapdev02/psswebapi_dms/"
        var okHttpClient = buildClient(context, formBody, authorization)


        apiObservables = LruCache<Class<*>, Observable<*>>(10)

        val retrofit = Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(HelpersJava.getCustomGson(context)))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build()

        service = retrofit.create(NetworkService::class.java)
    }

    constructor(context: Context, formBody: RequestBody) {

        val okHttpClient = buildClient(context, formBody)
        apiObservables = LruCache<Class<*>, Observable<*>>(10)

        val retrofit = Retrofit.Builder()
                .baseUrl(Const.url)
                .addConverterFactory(GsonConverterFactory.create(HelpersJava.getCustomGson(context)))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build()

        service = retrofit.create(NetworkService::class.java)
    }

    private val retroClient: Retrofit
        get() = Retrofit.Builder()
                .baseUrl(Const.url)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

    val apiService: NetworkService
        get() = retroClient.create(NetworkService::class.java)

    /**
     * Method to build and return an OkHttpClient so we can set/get
     * headers quickly and efficiently.

     * @return
     */
    fun buildClient(context: Context, formBody: RequestBody, authorization: String): OkHttpClient {

        //final String json = "[{\"LastSyncDate\": \"2000-01-01 00:00:00\", \"ModuleName\": \"Movies\"}, {\"LastSyncDate\": \"2000-01-01 00:00:00\", \"ModuleName\": \"TopBoxOffice\"}, {\"LastSyncDate\": \"2000-01-01 00:00:00\", \"ModuleName\": \"OpeningThisWeek\"}, {\"LastSyncDate\": \"2000-01-01 00:00:00\", \"ModuleName\": \"Upcoming\"}, {\"LastSyncDate\": \"2000-01-01 00:00:00\", \"ModuleName\": \"Cities\"}, {\"LastSyncDate\": \"2000-01-01 00:00:00\", \"ModuleName\": \"PlayingAt\"}, {\"LastSyncDate\": \"2000-01-01 00:00:00\", \"ModuleName\": \"Theaters\"}]";

        val builder = OkHttpClient.Builder()

//        val interceptor = HttpLoggingInterceptor()
//        interceptor.level = HttpLoggingInterceptor.Level.BODY
//
//        builder.addInterceptor(interceptor)

        builder.addInterceptor(Interceptor { chain ->

            val request = chain.request().newBuilder()
//                    .addHeader("Accept", "application/json")
                    .addHeader("content-type", "application/json")
                    .addHeader("Authorization", authorization)
                    .addHeader("HeadOfficeCode", preferenceManager.getValueFromSecurePreferences(
                            preferenceManager, context.getString(R.string.secure_preferences_head_office_code),
                            context))
                    //.addHeader("Content-Encoding", "gzip")
                    //.addHeader("Transfer-Encoding","chunked")
                    .post(formBody)
                    .build()

            return@Interceptor chain.proceed(request)
        })

        builder.readTimeout(300, TimeUnit.SECONDS)
        builder.connectTimeout(300, TimeUnit.SECONDS)
        //builder.certificatePinner(sertificatePinning())
        builder.addInterceptor(ChuckInterceptor(context))
        return builder.build()
    }
    fun buildClientGet(context: Context): OkHttpClient {

        //final String json = "[{\"LastSyncDate\": \"2000-01-01 00:00:00\", \"ModuleName\": \"Movies\"}, {\"LastSyncDate\": \"2000-01-01 00:00:00\", \"ModuleName\": \"TopBoxOffice\"}, {\"LastSyncDate\": \"2000-01-01 00:00:00\", \"ModuleName\": \"OpeningThisWeek\"}, {\"LastSyncDate\": \"2000-01-01 00:00:00\", \"ModuleName\": \"Upcoming\"}, {\"LastSyncDate\": \"2000-01-01 00:00:00\", \"ModuleName\": \"Cities\"}, {\"LastSyncDate\": \"2000-01-01 00:00:00\", \"ModuleName\": \"PlayingAt\"}, {\"LastSyncDate\": \"2000-01-01 00:00:00\", \"ModuleName\": \"Theaters\"}]";

        val builder = OkHttpClient.Builder()

//        val interceptor = HttpLoggingInterceptor()
//        interceptor.level = HttpLoggingInterceptor.Level.BODY
//
//        builder.addInterceptor(interceptor)

        builder.addInterceptor(Interceptor { chain ->

            val request = chain.request().newBuilder()
//                    .addHeader("Accept", "application/json")
                   // .addHeader("content-type", "application/json")
                    //.addHeader("Authorization", authorization)
//                    .addHeader("HeadOfficeCode", preferenceManager.getValueFromSecurePreferences(
//                            preferenceManager, context.getString(R.string.secure_preferences_head_office_code),
//                            context))
                    //.addHeader("Content-Encoding", "gzip")
                    //.addHeader("Transfer-Encoding","chunked")
                    .get()
                    .build()
//            try {
//                return@Interceptor chain.proceed(request)
//            } catch (ex: Exception) {
//                //FirebaseCrash.report(ex);
//                //FirebaseCrash.logcat(Log.ERROR, TAG, ex.getMessage());
//                return@Interceptor null
//            }

            return@Interceptor chain.proceed(request)
        })

        builder.readTimeout(300, TimeUnit.SECONDS)
        builder.connectTimeout(300, TimeUnit.SECONDS)
        //builder.certificatePinner(sertificatePinning())
        builder.addInterceptor(ChuckInterceptor(context))
        return builder.build()
    }
//    private fun sertificatePinning(): CertificatePinner {
//        val certPinner = CertificatePinner.Builder()
//                .add("devproxy.astra.co.id", Const.firstSha256)
//                .add("devproxy.astra.co.id", Const.secondSha256)
//
//        return certPinner.build()
//    }

    fun buildClient(context: Context, formBody: RequestBody): OkHttpClient {

        //final String json = "[{\"LastSyncDate\": \"2000-01-01 00:00:00\", \"ModuleName\": \"Movies\"}, {\"LastSyncDate\": \"2000-01-01 00:00:00\", \"ModuleName\": \"TopBoxOffice\"}, {\"LastSyncDate\": \"2000-01-01 00:00:00\", \"ModuleName\": \"OpeningThisWeek\"}, {\"LastSyncDate\": \"2000-01-01 00:00:00\", \"ModuleName\": \"Upcoming\"}, {\"LastSyncDate\": \"2000-01-01 00:00:00\", \"ModuleName\": \"Cities\"}, {\"LastSyncDate\": \"2000-01-01 00:00:00\", \"ModuleName\": \"PlayingAt\"}, {\"LastSyncDate\": \"2000-01-01 00:00:00\", \"ModuleName\": \"Theaters\"}]";

        val builder = OkHttpClient.Builder()

        builder.addInterceptor(Interceptor { chain ->

            val request = chain.request().newBuilder()
//                    .addHeader("Accept", "application/json")
                    .addHeader("Content-Type", "text/plain")
                    //.addHeader("Accept-Encoding", "gzip")
                    //.addHeader("Content-Encoding", "gzip")
                    //.addHeader("Transfer-Encoding","chunked")
                    .post(formBody)
                    .build()
//            try {
//                return@Interceptor chain.proceed(request)
//            } catch (ex: Exception) {
//                //FirebaseCrash.report(ex);
//                //FirebaseCrash.logcat(Log.ERROR, TAG, ex.getMessage());
//                return@Interceptor null
//            }

            return@Interceptor chain.proceed(request)
        })

        builder.readTimeout(300, TimeUnit.SECONDS)
        builder.connectTimeout(300, TimeUnit.SECONDS)
        builder.addInterceptor(ChuckInterceptor(context))
        return builder.build()
    }


    /**
     * Method to clear the entire cache of observables
     */
    fun clearCache() {
        apiObservables.evictAll()
    }


    /**
     * Method to either return a cached observable or prepare a new one.

     * @param unPreparedObservable
     * *
     * @param clazz
     * *
     * @param cacheObservable
     * *
     * @param useCache
     * *
     * @return Observable ready to be subscribed to
     */
    fun getPreparedObservable(unPreparedObservable: Observable<*>, clazz: Class<*>, cacheObservable: Boolean, useCache: Boolean): Observable<*> {

        var preparedObservable: Observable<*>? = null

        if (useCache)
        //this way we don't reset anything in the cache if this is the only instance of us not wanting to use it.
            preparedObservable = apiObservables.get(clazz)

        if (preparedObservable != null)
            return preparedObservable

        //we are here because we have never created this observable before or we didn't want to use the cache...

        preparedObservable = unPreparedObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())

        if (cacheObservable) {
            preparedObservable = preparedObservable!!.cache()
            apiObservables.put(clazz, preparedObservable!!)
        }

        return preparedObservable
    }
}
