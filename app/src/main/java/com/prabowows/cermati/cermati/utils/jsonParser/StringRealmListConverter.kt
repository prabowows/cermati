package id.astra.hso.stowadig.utils.jsonParser


import com.google.gson.JsonArray
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import com.prabowows.cermati.cermati.models.ListString


import java.lang.reflect.Type

import io.realm.RealmList

/**
 * Created by Prabowo Wahyu Sudarno.
 */

class StringRealmListConverter : JsonSerializer<RealmList<ListString>>, JsonDeserializer<RealmList<ListString>> {

    override fun serialize(src: RealmList<ListString>, typeOfSrc: Type,
                           context: JsonSerializationContext): JsonElement {
        val ja = JsonArray()
        for (tag in src) {
            ja.add(context.serialize(tag))
        }
        return ja
    }

    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type,
                             context: JsonDeserializationContext): RealmList<ListString> {
        val realmList = RealmList<ListString>()
        val ja = json.asJsonArray
        for (je in ja) {
            val value = context.deserialize<String>(je, String::class.java)
            val list = ListString()
            list.value = value
            realmList.add(list)
        }
        return realmList
    }

}
