package com.prabowows.cermati.cermati.modules.main

import android.content.Intent

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import id.ai.mobilerecruitment.utils.Helpers
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*

import com.jakewharton.rxbinding.widget.RxTextView
import com.prabowows.cermati.cermati.R
import com.prabowows.cermati.cermati.models.Item
import com.prabowows.cermati.cermati.models.response.GithubResponse
import rx.android.schedulers.AndroidSchedulers
import java.util.ArrayList
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity(), OnLoadMoreListener {

    private lateinit var presenter: MainPresenter
    private lateinit var realm: Realm
    private val list: ArrayList<Item> = arrayListOf()
    private var firstloaded = false
    private lateinit var adapter: MainAdapter




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Example of a call to a native method


        realm = Realm.getDefaultInstance()
        presenter = MainPresenter(this, realm)

        firstloaded = false

        RxTextView.textChangeEvents(et_search)
            .debounce(
                750,
                TimeUnit.MILLISECONDS
            ) // Better store the value in a constant like Constant.DEBOUNCE_SEARCH_REQUEST_TIMEOUT
//            .map { it.text().toString() }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (et_search.text!!.isNotEmpty()) {
                    list.clear()
                    firstloaded = true
                    presenter.hitGetData(it.text().toString())
                    Helpers.showProgress(this)
                }
            }

        rv_github.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_github.setHasFixedSize(true)

        adapter = MainAdapter(
            rv_github,
            this,
            list,
            this
        ) {

        }

        rv_github.adapter = adapter






    }

    override fun onResume() {
        super.onResume()

//        firstloaded = false
//        list.clear()
//
//
//        RxTextView.textChangeEvents(et_search)
//            .debounce(
//                750,
//                TimeUnit.MILLISECONDS
//            ) // Better store the value in a constant like Constant.DEBOUNCE_SEARCH_REQUEST_TIMEOUT
////            .map { it.text().toString() }
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe {
//                if (et_search.text!!.isNotEmpty()) {
//                    presenter.hitGetData(it.text().toString())
//                    Helpers.showProgress(this)
//                }
//            }

    }


    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    external fun stringFromJNI(): String

    companion object {

        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("native-lib")
        }
    }

    fun onResult(response: GithubResponse) {
        Helpers.dismissProgress(this)

        if(list.size==0){
            firstloaded = true
        }

        if (firstloaded) {
            if (!list.isEmpty()) {
                this.list.removeAt(this.list.size - 1)
                adapter.notifyItemRemoved(this.list.size)
            }
//            else if(list.size==0){
//                this.list.removeAt(this.list.size - 1)
//                adapter.notifyItemRemoved(this.list.size)
//            }
        }

//        if (!firstloaded) {
//            this.list.clear()
//            //this.list.addAll(response.items!!)
//                        for (i in response.items!!.indices){
//                if(i<10){
//                    this.list.add(response.items!![i])
//                }
//            }
//            setAdapter(list)
//            //lottie_inbox.visibility = View.VISIBLE
//            //rv_github.visibility = View.INVISIBLE
//        }


        if (response.items!!.isNotEmpty()) {
//            response.items!!.forEach {
//                this.list.add(it)
//            }
            for (i in response.items!!.indices){
                if(i<10){
                    this.list.add(response.items!![i])
                }
            }
        }


        if(response.items!!.size == 0){
            setAdapter(list)
            lottie_data.visibility = View.VISIBLE
        }
        else {
            setAdapter(list)
            lottie_data.visibility = View.GONE

        }

    }

    fun setAdapter(listResult: MutableList<Item>) {

        Helpers.dismissProgress(this)

        if(listResult.size == 0){
            //setAdapter(response.items!!)
            lottie_data.visibility = View.VISIBLE
        }
        else {
            //setAdapter(response.items!!)
            lottie_data.visibility = View.GONE

        }


        if (firstloaded) {
            if (!list.isEmpty()) {
                this.list.removeAt(this.list.size - 1)
                adapter.notifyItemRemoved(this.list.size)
            }
        }

        if (!firstloaded && listResult.isEmpty()) {
            lottie_data.visibility = View.VISIBLE
            rv_github.visibility = View.INVISIBLE
        }


        if (listResult.isNotEmpty()) {
            listResult.forEach {
                this.list.add(it)
            }
        } else {

        }

        firstloaded = true
        adapter.notifyDataSetChanged()
        adapter.finishLoaded()

    }


    fun onError(response: String) {

        Helpers.showDialogInfo(this,
            R.string.alert, getString(R.string.close), "Please check your connection and try again one minute later")

        Helpers.dismissProgress(this)
    }

    override fun onLoadMore() {
        val messageItem: Item = Item()
        messageItem.statusForLoading = true
        this.list.add(messageItem)
        adapter.notifyItemInserted(list.size-1)
        Handler().postDelayed({
            RxTextView.textChangeEvents(et_search)
                .debounce(
                    750,
                    TimeUnit.MILLISECONDS
                ) // Better store the value in a constant like Constant.DEBOUNCE_SEARCH_REQUEST_TIMEOUT
//            .map { it.text().toString() }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (et_search.text!!.isNotEmpty()) {
                        presenter.hitGetData(it.text().toString())
                       // Helpers.showProgress(this)
                    }
                }

        }, 2000)
    }

}
