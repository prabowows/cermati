package com.prabowows.cermati.cermati.modules.main

import com.example.agrprabo6477.ajobthing.utils.PreferenceManager
import com.prabowows.cermati.cermati.models.Item
import com.prabowows.cermati.cermati.models.response.GithubResponse
import id.astra.daihatsu.mobileservice.networking.NetworkModule
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.android.schedulers.AndroidSchedulers
import io.realm.Realm



class MainPresenter(val v: MainActivity, mRealm: Realm) {

    private var service: NetworkModule? = null
    private val mCompositeDisposable: CompositeDisposable?
    private val realm = mRealm

    private val preferenceManager = PreferenceManager.instance

    init {
        this.mCompositeDisposable = CompositeDisposable()
    }


    fun hitGetData(user : String) {

        try {
            //val requestToJson: String = Helpers.getDefaultGson(v.requireContext()).toJson(request)

            this.service = NetworkModule(
                v
            )
            val responseObservable = service!!.getPreparedObservable(
                service!!.service.GetData(user), GithubResponse::class.java, false, false
            )
            mCompositeDisposable!!.add(
                responseObservable
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ this.handleResponse(it) }, { this.handleResponse(it) })
            )
        } catch (e: Exception) {
            e.printStackTrace()
            //Crashlytics.logException(e)
        }
    }


    fun handleResponse(response: Any) {
        try {
            if (response is GithubResponse) {
                var list: MutableList<Item> = mutableListOf()
                list.addAll(response.items!!)
                v.setAdapter(list)

//                if (response.status.equals("success", true)) {
//
//
//
//                } else {
//                    v.onError("System Error")
//                }
            } else if (response is Throwable) {
                v.onError(response.message!!)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}

