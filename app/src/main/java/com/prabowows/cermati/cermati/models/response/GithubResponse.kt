package com.prabowows.cermati.cermati.models.response

import com.google.gson.annotations.SerializedName
import com.prabowows.cermati.cermati.models.Item

class GithubResponse {
    @SerializedName("incomplete_results")
    var incompleteResults: Boolean?= false
    @SerializedName("items")
    var items: List<Item>? = arrayListOf()
    @SerializedName("total_count")
    var totalCount: Int? = 0
}