package com.prabowows.cermati.cermati.modules.main

interface OnLoadMoreListener {
    fun onLoadMore()
}