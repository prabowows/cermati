package id.astra.daihatsu.mobileservice.networking


import com.prabowows.cermati.cermati.models.response.GithubResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


interface NetworkService {
    @GET("users")
    fun GetData(@Query("q") userId: String): Observable<GithubResponse>

}
