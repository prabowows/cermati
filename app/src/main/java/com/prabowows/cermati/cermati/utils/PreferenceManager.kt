package com.example.agrprabo6477.ajobthing.utils

import android.content.Context
import com.prabowows.cermati.cermati.R
import com.securepreferences.SecurePreferences


open class PreferenceManager private constructor() {

    var securePreferences: SecurePreferences? = null
    var accessToken: String? = null
    var refresh_Token: String? = null
    var headOfficeCode: String? = null
    var authUsername: String? = null

    init {
    }

    private object Holder {
        val instances = PreferenceManager()
    }

    companion object {
        val instance: PreferenceManager by lazy { Holder.instances }
    }

    private fun getSecurePreference(preferences: PreferenceManager, context: Context): SecurePreferences? {
        if (preferences.securePreferences == null) {
            preferences.securePreferences = SecurePreferences(context)
        }
        return preferences.securePreferences
    }

    fun clearPreference(preferences: PreferenceManager, context: Context) {
        val secureSharedPreferences = preferences.getSecurePreference(preferences, context)
        secureSharedPreferences!!.edit().clear().apply()
    }

    fun getStringValue(preferences: PreferenceManager, keyword: String, context: Context): String? {
        var values: String? = null
        val secureSharedPreferences = preferences.getSecurePreference(preferences, context)

        values = secureSharedPreferences!!.getString(keyword, "")

        return values
    }

    fun getBooleanValue(preferences: PreferenceManager, keyword: String, context: Context): Boolean? {
        var values: Boolean? = false
        val secureSharedPreferences = preferences.getSecurePreference(preferences, context)

        values = secureSharedPreferences!!.getBoolean(keyword, false)

        return values
    }

    fun getIntValue(preferences: PreferenceManager, keyword: String, context: Context): Int? {
        var values: Int? = 0
        val secureSharedPreferences = preferences.getSecurePreference(preferences, context)

        values = secureSharedPreferences!!.getInt(keyword, 0)

        return values
    }

    fun setStringValue(preferences: PreferenceManager,
                                    keyword: String, value: String, context: Context) {
        try {
            val secureSharedPreferences = preferences.getSecurePreference(preferences, context)
            secureSharedPreferences!!.edit().putString(keyword, value).apply()

        } catch (ex: Exception) {
            throw ex
        }
    }

    fun setBooleanValue(preferences: PreferenceManager,
                       keyword: String, value: Boolean, context: Context) {
        try {
            val secureSharedPreferences = preferences.getSecurePreference(preferences, context)
            secureSharedPreferences!!.edit().putBoolean(keyword, value).apply()

        } catch (ex: Exception) {
            throw ex
        }
    }

    fun setIntValue(preferences: PreferenceManager,
                        keyword: String, value: Int, context: Context) {
        try {
            val secureSharedPreferences = preferences.getSecurePreference(preferences, context)
            secureSharedPreferences!!.edit().putInt(keyword, value).apply()

        } catch (ex: Exception) {
            throw ex
        }
    }

    fun getValueFromSecurePreferences(preferences: PreferenceManager,
                                      keyword: String, context: Context): String? {
        var values: String? = null
        val secureSharedPreferences = preferences.getSecurePreference(preferences, context)
        when (keyword) {
            context.getString(R.string.secure_preferences_access_token) -> {
                if (preferences.accessToken == null) {
                    preferences.accessToken = secureSharedPreferences!!.getString(keyword, "")
                }
                values = preferences.accessToken
            }
            context.getString(R.string.secure_preferences_refresh_token) -> {
                if (preferences.refresh_Token == null) {
                    preferences.refresh_Token = secureSharedPreferences!!.getString(keyword, "")
                }
                values = preferences.refresh_Token
            }
            context.getString(R.string.secure_preferences_head_office_code) -> {
                if (preferences.headOfficeCode == null) {
                    preferences.headOfficeCode = secureSharedPreferences!!.getString(keyword, "")
                }
                values = preferences.headOfficeCode
            }
            context.getString(R.string.secure_preferences_username) -> {
                if (preferences.authUsername == null) {
                    preferences.authUsername = secureSharedPreferences!!.getString(keyword, "")
                }
                values = preferences.authUsername
            }
        }

        return values
    }

    fun setValueToSecurePreferences(preferences: PreferenceManager,
                                    keyword: String, value: String, context: Context) {
        try {
            val secureSharedPreferences = preferences.getSecurePreference(preferences, context)
            secureSharedPreferences!!.edit().putString(keyword, value).apply()
            when (keyword) {
                context.getString(R.string.secure_preferences_access_token) -> {
                    preferences.accessToken = value
                }
                context.getString(R.string.secure_preferences_refresh_token) -> {
                    preferences.refresh_Token = value
                }
                context.getString(R.string.secure_preferences_head_office_code) -> {
                    preferences.headOfficeCode = value
                }
                context.getString(R.string.secure_preferences_username) -> {
                    preferences.authUsername = value
                }
            }
        } catch (ex: Exception) {
            throw ex
        }
    }
}

