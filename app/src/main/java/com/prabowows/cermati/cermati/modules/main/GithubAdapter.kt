package com.prabowows.cermati.cermati.modules.main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.prabowows.cermati.cermati.R
import com.prabowows.cermati.cermati.models.Item
import id.ai.mobilerecruitment.utils.Helpers
import kotlinx.android.synthetic.main.item_github.view.*

class GithubAdapter(
    private val listMenu: List<Item>,
    private val context: Context,
    private val itemClick: (data: Item, pos: Int) -> Unit,
    private val loveClick: (data: Item, pos: Int) -> Unit
) :
    RecyclerView.Adapter<GithubAdapter.Holder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_github, parent, false)

        return Holder(view!!, context, itemClick, loveClick)
    }

    override fun getItemCount(): Int {
        return listMenu.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.Bind(listMenu[position], position)
    }

    class Holder(
        view: View,
        val context: Context,
        val itemClick: (data: Item, pos: Int) -> Unit,
        val loveClick: (data: Item, pos: Int) -> Unit
    ) :
        RecyclerView.ViewHolder(view) {


        fun Bind(data: Item, pos: Int) {
            with(data) {
               Helpers.showClearImageFromURL(context,data.avatarUrl!!,itemView.iv_github)
                itemView.tv_github.text = data.login

            }
        }
    }
}


