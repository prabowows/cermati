package com.prabowows.cermati.cermati.models.response


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by agrprabo6477 on 05/11/2018.
 */

class ErrorResponse {
    @SerializedName("error")
    @Expose
    var error: String = ""

    @SerializedName("error_description")
    @Expose
    var errorDesription: String = ""

    @SerializedName("message")
    @Expose
    var message: String = ""
}