package com.prabowows.cermati.cermati

import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import id.ai.mobilerecruitment.utils.Helpers


/**
 * Created by Prabowo Wahyu Sudarno.
 */
class App : MultiDexApplication() {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        Helpers.setRealmConfiguration(this)
        //Fabric.with(this, Crashlytics())
    }
}