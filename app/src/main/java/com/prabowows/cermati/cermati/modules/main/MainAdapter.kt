package com.prabowows.cermati.cermati.modules.main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.prabowows.cermati.cermati.R
import com.prabowows.cermati.cermati.models.Item
import io.realm.Realm
import kotlinx.android.synthetic.main.item_github.view.*
import java.text.SimpleDateFormat
import java.util.*
import id.ai.mobilerecruitment.utils.Helpers


class MainAdapter(
    private val recyclerView: RecyclerView,
    private val onLoadMoreListener: OnLoadMoreListener,
    private val list: MutableList<Item>,
    private val context: Context,
    private val itemClick: (Item) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var isLoading: Boolean = false
    private val visibleThreshold = 10
    private var lastVisibleItem: Int = 0
    private var totalItemCount: Int = 0
    private val VIEW_TYPE_ITEM = 0
    private val VIEW_TYPE_LOADING = 1

    init {
        val linearLayoutManager: LinearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()
                totalItemCount = linearLayoutManager.itemCount
                if (list.size >= visibleThreshold) {
                    if (!isLoading && totalItemCount <= lastVisibleItem + visibleThreshold) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore()
                        }
                        isLoading = true
                    }
                }
            }
        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        if (viewType == VIEW_TYPE_ITEM) {
            val itemView: View = LayoutInflater.from(parent!!.context).inflate(R.layout.item_github, parent, false)
            viewHolder = Holder(itemView, itemClick)
        } else if (viewType == VIEW_TYPE_LOADING) {
            val itemView: View = LayoutInflater.from(parent!!.context).inflate(R.layout.item_loading, parent, false)
            viewHolder = LoadingViewHolder(itemView)
        }
        return viewHolder!!
    }

    override fun getItemCount(): Int {
        return if (list == null)
            0
        else
            list.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LoadingViewHolder) {
            holder.progressBar.isIndeterminate = true
        } else if (holder is Holder) {
            holder.bind(list[position], position, context)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (list.get(position).statusForLoading)
            VIEW_TYPE_LOADING
        else
            VIEW_TYPE_ITEM
    }

    fun finishLoaded() {
        isLoading = false
    }

    class LoadingViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var progressBar: ProgressBar

        init {
            progressBar = view.findViewById<View>(R.id.progressBar1) as ProgressBar
        }
    }


    class Holder(
        val view: View,
        private val itemClick: (Item) -> Unit
    ) : RecyclerView.ViewHolder(view) {

        fun bind(data: Item, position: Int, context: Context) {
            with(data) {

//                itemView.tv_inbox_subject.text = data.subject
//                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
//                    itemView.tv_inbox_message.text = android.text.Html.fromHtml(data.message, android.text.Html.FROM_HTML_MODE_LEGACY).toString()
//                } else {
//                    itemView.tv_inbox_message.text = android.text.Html.fromHtml(data.message).toString()
//                }
//                if (data.hasRead) {
//                    itemView.view_item_inbox.setBackgroundColor(androidx.core.content.ContextCompat.getColor(context, R.color.md_white_1000))
//                } else {
//                    itemView.view_item_inbox.setBackgroundColor(
//                        androidx.core.content.ContextCompat.getColor(
//                            context,
//                            R.color.color_grey_unread_inbox
//                        )
//                    )
//                }
////                Helpers.showImageFromURL(context,data,itemView.iv_gallery)
//
//                itemView.tv_inbox_time.text = Helpers.getDistanceTimeReadMessage(data.createDate!!)
                Helpers.showClearImageFromURL(context,data.avatarUrl!!,itemView.iv_github)
                itemView.tv_github.text = data.login
                itemView.setOnClickListener {
                    itemClick(this)
                }


            }

        }
    }
}